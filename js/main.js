$(function () {

    var allValues = ['red', 'blue', 'green', 'yellow', 'violet', 'brown', 'purple', 'black', 'white'];
    tokenfieldInitilize('#tokenfield', '.hints', allValues);

});

function tokenfieldInitilize(tokenfieldSelector, hintSelector, dataHint) {

    var hintsNode = $(hintSelector);
    var tokenfieldNode = $(tokenfieldSelector);

    $.each(dataHint, function(i, item) {
        var el = $('<li class="token"></li>');
        var valueEl = $('<span class="token-label"></span>')
            .text(item)
            .click( function() {
                tokenfieldNode.tokenfield('createToken', item);
            });
        el.append(valueEl);
        hintsNode.append(el);
    });

    var hintTokens = $('.hints .token-label');
    
    tokenfieldNode
        .on('tokenfield:createdtoken', function (event) {
            $.each(hintTokens, function(i, el) {
                var $el = $(el);
                if ($el.text() == event.attrs.value) {
                    $el.parent().addClass('disabled');
                }
            });
        })
        .on('tokenfield:removedtoken', function (event) {
            $.each(hintTokens, function (i, el) {
                var $el = $(el);
                if ($el.text() == event.attrs.value) {
                    $el.parent().removeClass('disabled');
                }
            });
        })
        .tokenfield();

}

